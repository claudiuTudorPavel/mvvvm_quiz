﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using wpfQuizz.ViewModels.Commands;
using wpfQuizz.Views;

namespace wpfQuizz.ViewModels
{
	public class LoadLoginViewModel : Screen
	{


		private QuizViewModel QuizViewModel;

		
		private string _userID;

		public string UserID
		{
			get
			{
				return _userID;
			}
			set
			{
				_userID = value;
				NotifyOfPropertyChange(() => UserID);
			}
		}

		private string _userPassword;

		public string UserPassword
		{
			get
			{
				return _userPassword;
			}
			set
			{
				_userPassword = value;
				NotifyOfPropertyChange(() => UserPassword);
			}
		}

		public void CheckUserPassword(string userID, string userPassword)
		{
			if (String.IsNullOrWhiteSpace(SelectedDifficulty))
			{
				MessageBox.Show("Please choose level of difficulty");
			}
			else
			{
				XmlDocument xmlDocument = generateXML();
				if (checkUserAndPassword(xmlDocument) == true)
				{
					MessageBox.Show("You have entered correct details");
					LoadQuizView();
				}
				else
				{
					MessageBox.Show("User or password is incorrect!");
				}
			}
			
		}

		public Boolean checkUserAndPassword(XmlDocument xmlDoc)
		{
			Boolean realAccount = false;

			List<String> passList = loadPasswords(xmlDoc);
			List<String> userList = loadUsers(xmlDoc);

			String user = _userID;
			String password = _userPassword;

			for (int i = 0; i < passList.Count; i++)
			{
				if (passList[i] == password && userList[i] == user)
				{
					return true;
				}
			}

			return realAccount;
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");

			return xmlDoc;
		}

		public List<String> loadPasswords(XmlDocument xmlDoc)
		{
			List<String> passList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;

			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userpassword[@numberpass='" + index + "']");
				temp = tempUserIDNode.InnerText;

				passList.Add(temp);
				index++;
			}

			return passList;
		}

		public List<String> loadUsers(XmlDocument xmlDoc)
		{
			List<String> userList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;


			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userid[@number='" + index + "']");
				temp = tempUserIDNode.InnerText;

				userList.Add(temp);
				index++;
			}

			return userList;
		}

		public static double countUserIds()
		{
			double userCount;
			var xDoc = XDocument.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");
			userCount = (double)xDoc.XPathEvaluate("count(//accounts/account/userid)");

			return userCount;
		}

		
		private BindableCollection<String> _difficulty = new BindableCollection<string>();

		public BindableCollection<String> Difficulty
		{
			get { return _difficulty; }
			set { _difficulty = value; }
		}

		private string _selectedDifficulty;

		public string SelectedDifficulty
		{
			get { return _selectedDifficulty; }
			set { _selectedDifficulty = value; NotifyOfPropertyChange(() => SelectedDifficulty); }
		}


		public LoadLoginViewModel()
		{
			Difficulty.Add("easy");
			Difficulty.Add("medium");
			Difficulty.Add("hard");

			
			


		}

		public void LoadQuizView()
		{
			
			//MessageBox.Show(SelectedDifficulty);
			QuizViewModel = new QuizViewModel(SelectedDifficulty);
			QuizView quizView = new QuizView();
			quizView.DataContext = QuizViewModel;
			quizView.Show();
			this.TryClose();
		}

		
		public bool CanCheckUserPassword(string userID, string userPassword)
		{
			if(String.IsNullOrWhiteSpace(userID) || String.IsNullOrWhiteSpace(userPassword))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

	}
}
