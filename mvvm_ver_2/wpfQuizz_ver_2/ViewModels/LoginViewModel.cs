﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace wpfQuizz.ViewModels
{
	public class LoginViewModel : Conductor<object>
	{
		
		public void LoadLogin()
		{
			ActivateItem(new LoadLoginViewModel());
		}

		public void LoadRegistration()
		{
			ActivateItem(new LoadRegistrationViewModel());
		}

		

	}
}
