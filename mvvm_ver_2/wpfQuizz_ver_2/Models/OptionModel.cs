﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfQuizz.Models
{
	public class OptionModel
	{

		public string OptionText { get; set; }
		public string OptionTagText { get; set; }

	}
}
