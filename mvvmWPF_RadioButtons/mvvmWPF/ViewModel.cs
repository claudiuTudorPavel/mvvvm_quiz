﻿using System.ComponentModel;
using System.Windows.Input;

namespace mvvmWPF
{
	class ViewModel : INotifyPropertyChanged
	{
		private string _name;

		public string Name
		{
			get { return _name; }
			set { _name = value; OnPropertyChange("Name"); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChange(string propertyname)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
			}
		}
				
		public ICommand MyCommand { get; set; }

		private bool canexecutemethod(object parameter)
		{
			if (parameter != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private void executemethod(object parameter)
		{
			Name = (string)parameter;
		}


		public ViewModel()
		{
			//creez o comanda noua cu fiecare obiect de tip viewmodel creat
			MyCommand = new RelayCommand(executemethod, canexecutemethod);
		}

	}
}
