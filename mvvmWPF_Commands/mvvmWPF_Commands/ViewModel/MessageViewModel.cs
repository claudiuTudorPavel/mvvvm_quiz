﻿using mvvmWPF_Commands.ViewModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace mvvmWPF_Commands.ViewModel
{
	public class MessageViewModel
	{
		//public string MessageText { get; set; }
		public MessageCommand DispalyMessageCommand { get; private set; }

		public MessageViewModel()
		{
			DispalyMessageCommand = new MessageCommand(DisplayMessage);
		}

		public void DisplayMessage(string message)
		{
			MessageBox.Show(message);
		}

	}
}
