﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace wpfQuizz.ViewModels
{
	public class LoadLoginViewModel:Screen 
	{
		private string setting;

		private string _userID;

		public string UserID
		{
			get
			{
				return _userID;
			}
			set
			{
				_userID = value;
				NotifyOfPropertyChange(() => UserID);
			}
		}

		private string _userPassword;

		public string UserPassword
		{
			get
			{
				return _userPassword;
			}
			set
			{
				_userPassword = value;
				NotifyOfPropertyChange(() => UserPassword);
			}
		}

		public void CheckUserPassword(string userID, string userPassword)
		{
			XmlDocument xmlDocument = generateXML();
			if(checkUserAndPassword(xmlDocument) == true)
			{
				MessageBox.Show("You have entered correct details");
			}
			else
			{
				MessageBox.Show("User or password is incorrect!");
			}
		}

		public Boolean checkUserAndPassword(XmlDocument xmlDoc)
		{
			Boolean realAccount = false;

			List<String> passList = loadPasswords(xmlDoc);
			List<String> userList = loadUsers(xmlDoc);

			String user = _userID;
			String password = _userPassword;

			for (int i = 0; i < passList.Count; i++)
			{
				if (passList[i] == password && userList[i] == user)
				{
					return true;
				}
			}

			return realAccount;
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");

			return xmlDoc;
		}

		public List<String> loadPasswords(XmlDocument xmlDoc)
		{
			List<String> passList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;

			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userpassword[@numberpass='" + index + "']");
				temp = tempUserIDNode.InnerText;

				passList.Add(temp);
				index++;
			}

			return passList;
		}

		public List<String> loadUsers(XmlDocument xmlDoc)
		{
			List<String> userList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;


			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userid[@number='" + index + "']");
				temp = tempUserIDNode.InnerText;

				userList.Add(temp);
				index++;
			}

			return userList;
		}

		public static double countUserIds()
		{
			double userCount;
			var xDoc = XDocument.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");
			userCount = (double)xDoc.XPathEvaluate("count(//accounts/account/userid)");

			return userCount;
		}

		public void easySetting()
		{
			setting = "easy";
		}

	}
}
