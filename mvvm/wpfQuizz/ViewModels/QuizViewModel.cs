﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using wpfQuizz.Models;
using wpfQuizz.ViewModels.Commands;
using wpfQuizz.Views;

namespace wpfQuizz.ViewModels
{
	public class QuizViewModel: Screen
	{
		//bau bau
		//instantez o variabila de tip array in care incarc numere aleatorii generate
		//instantez un index i
		public int[] questions = randomArray();
		public RadioButton[] nextButtons;
		public int i = 0;
		public int j = 1;
		public int score;
		public int truePos;
		public XmlDocument xmlDoc = generateXML();
		private static string _setDifficulty = "medium";

		private string _questionText;
		private string _optionText;

		//proprietatile
		public string OptionText
		{
			get { return _optionText; }
			set { _optionText = value; NotifyOfPropertyChange(() => OptionText); }
		}
		public string QuestionText
		{
			get { return _questionText; }
			set { _questionText = value; NotifyOfPropertyChange(() => QuestionText); }
		}

			   		 	
		private BindableCollection<OptionModel> _option = new BindableCollection<OptionModel>();
		private BindableCollection<QuestionModel> _qestion = new BindableCollection<QuestionModel>();

		
		public BindableCollection<OptionModel> Options
				{
					get { return _option; }
					set { _option = value; NotifyOfPropertyChange(() => Options); }
				}
		public BindableCollection<QuestionModel> Questions
				{
					get { return _qestion; }
					set { _qestion = value; }
				}
		
		public QuestionModel questionTemp;

		public RadioButtonCommand radioButtonCommand { get; private set; }
		
		public static string  SetDifficulty
		{
			get { return _setDifficulty; }
			set { _setDifficulty = value; }
		}


		//logica pentru butonul next
		public void NextQuestion()
		{
			trackScore(nextButtons);
			i++;
			if (i<questions.Length)
			{
			
				questionTemp = new QuestionModel { QuestionText = generateQuestionText() };
				Questions.Add(questionTemp);

				nextButtons = generateButtons(xmlDoc);

				BindableCollection<OptionModel> nextOptions = generateOptions(nextButtons);
				
				QuestionText = questionTemp.QuestionText;

				Options = nextOptions;

				
			}
			else
			{
				MessageBox.Show("End of quizz!!");
			}
			
			
			
			MessageBox.Show("The score is " + trackScore(nextButtons).ToString());

		}



		public void CommandRadioButton()
		{
			
			if(PropertyValue)
				MessageBox.Show("bau bau bau");
			
			
		}


		private bool _propertyValue;

		public bool PropertyValue
		{
			get { return _propertyValue; }
			set { _propertyValue = value; NotifyOfPropertyChange(() => PropertyValue); }
		}





		public QuizViewModel()
		{
			radioButtonCommand = new RadioButtonCommand(CommandRadioButton);
			
			questionTemp = new QuestionModel { QuestionText = generateQuestionText() };
			Questions.Add(questionTemp);

			nextButtons = generateButtons(xmlDoc);

			BindableCollection<OptionModel> nextOptions = generateOptions(nextButtons);

			QuestionText = questionTemp.QuestionText;

			Options = nextOptions;

			
				
			
		}


			   		 	  	  	   	
		public BindableCollection<OptionModel> generateOptions(RadioButton[] radioButtons)
		{
			BindableCollection<OptionModel> tempOptions = new BindableCollection<OptionModel>();

			for (int d = 0; d < radioButtons.Length; d++)
			{
				OptionModel temp;
				temp = new OptionModel { OptionText = radioButtons[d].Content.ToString()};
				tempOptions.Add(temp);
				
			}

			return tempOptions;
		}

		private string generateQuestionText()
		{
			
			return generateQuestionNode(xmlDoc).InnerText;
			
		}
				
		//generez 5 numere aleatoare de la 1 la 10
		public static List<int> randomListGenerator()
		{
			List<int> randomList = new List<int>();
			Random generator = new Random();
			int myNumber = generator.Next(1, 10);
			int h = 0;

			while (h < 5)
			{
				if (!randomList.Contains(myNumber))
				{
					randomList.Add(myNumber);
					h++;
				}
				myNumber = generator.Next(1, 10);


			}

			return randomList;
		}

		//stochez valorile intr-un array
		public static int[] randomArray()
		{
			List<int> futureArray = randomListGenerator();
			int[] arr = new int[futureArray.Count];

			for (int k = 0; k < arr.Length; k++)
			{
				arr[k] = futureArray[k];

			}

			return arr;
		}

		//incarc un xmlDoc 
		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\QuizzQuestions.xml");

			return xmlDoc;
		}

		//accesez un singur node din xml doc in functie de dificultatea aleasa de utilizator
		//accesez una din intrebarile al carui numar este stocat in randomArray
		public XmlNode generateQuestionNode(XmlDocument xmlDoc)
		{
			XmlNode titleNode = xmlDoc.SelectSingleNode("//quizz/" + SetDifficulty + "/question[@number='" + questions[i] + "']/text");
			return titleNode;
		}
		
		//generez numarul de optiuni prezente la intrebarea aleasa
		public static double numberOfOptions(int[] questions, int i)
		{

			var xDoc = XDocument.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\QuizzQuestions.xml");
			double number = (double)xDoc.XPathEvaluate("count(//quizz/" + SetDifficulty + "/question[@number='" + questions[i] + "']/option)");
			
			return number;
		}


		//genereaza un array de radiobuttons 
		public RadioButton[] generateButtons(XmlDocument xmlDoc)
		{
			//calculam cate optiuni are intrebarea
			int optionsNumber = (int)numberOfOptions(questions, i);
			
			//creez array cu numar necesar de butoane 
			RadioButton[] radioButtons = new RadioButton[optionsNumber];
			

			//iterez prin array si creez butoane pentru fiecare pozitie
			for (int pos = 0; pos < optionsNumber; pos++)
			{
				XmlNode tempOptionNode = xmlDoc.SelectSingleNode("//quizz/" + SetDifficulty + "/question[@number='" + questions[i] + "']/option[@number='" + j + "']");
				radioButtons[pos] = new RadioButton();
				radioButtons[pos].Content = tempOptionNode.InnerText;
				
				radioButtons[pos].Margin = new Thickness(15, 35 + pos * 25, 0, 0);
				//caut pozitia raspunsului corect
				if (tempOptionNode.Attributes["correct"].Value == "true")
				{
					truePos = j;
					MessageBox.Show("Corect answer is " + truePos.ToString());

				}

				j++;
			}

			j = 1;

			return radioButtons;
		}

		public int trackScore(RadioButton[] radioButtons)
		{
			for (int s = 0; s < radioButtons.Length; s++)
			{
				if (radioButtons[s].IsChecked == true)
				{
					if (s + 1 == truePos)
					{
						MessageBox.Show("Corect answer has been selected");
						score++;
					}


				}
			}

			return score;
		}

	}
}
