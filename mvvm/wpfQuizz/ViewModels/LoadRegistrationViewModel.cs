﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using wpfQuizz.Views;

namespace wpfQuizz.ViewModels
{
	public class LoadRegistrationViewModel: Conductor<object>
	{
		private string _newUserID;
		private string _newUserPassword;
		private string _userMail;

		public string NewUserID
		{
			get
			{
				return _newUserID;
			}
			set
			{
				_newUserID = value;
				NotifyOfPropertyChange(() => NewUserID);
			}
		}
		
		public string NewUserPassword
		{
			get
			{
				return _newUserPassword;
			}
			set
			{
				_newUserPassword = value;
				NotifyOfPropertyChange(() => NewUserPassword);
			}
		}

		public string UserMail
		{
			get
			{
				return _userMail;
			}
			set
			{
				_userMail = value;
				NotifyOfPropertyChange(() => UserMail);
			}
		}


		public Boolean checkUserID(XmlDocument xmlDoc)
		{
			Boolean existingAccount = false;

			List<String> userList = loadUsers(xmlDoc);
			String user = _newUserID;

			for(int i=0; i<userList.Count; i++)
			{
				if(userList[i] == user)
				{
					return true; 
				}
			}

			return existingAccount;
		}

		public List<String> loadUsers(XmlDocument xmlDoc)
		{
			List<String> userList = new List<string>();
			String temp = "";
			int userCount = (int)countUserIds();
			int index = 1;


			while (index < userCount + 1)
			{
				XmlNode tempUserIDNode = xmlDoc.SelectSingleNode("//accounts/account/userid[@number='" + index + "']");
				temp = tempUserIDNode.InnerText;

				userList.Add(temp);
				index++;
			}

			return userList;
		}

		public static double countUserIds()
		{
			double userCount;
			var xDoc = XDocument.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");
			userCount = (double)xDoc.XPathEvaluate("count(//accounts/account/userid)");

			return userCount;
		}

		public static XmlDocument generateXML()
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");

			return xmlDoc;
		}

		public void RegisterNewUser()
		{
			XmlDocument xmlDocument = generateXML();
			if(checkUserID(xmlDocument)==false)
			{
				addNewUser();
				MessageBox.Show("New user account has been created.");
				QuizView quiz = new QuizView();
				quiz.Show();
				TryClose();
				//new QuizViewModel("easy");
			}
			else
			{
				MessageBox.Show("Please choose an unregistered User ID!");
			}
		}

		public void addNewUser()
		{
			XmlDocument xmlDocument = generateXML();

			XmlNode account = xmlDocument.CreateElement("account");
			XmlNode userID = xmlDocument.CreateElement("userid");
			XmlNode userEmail = xmlDocument.CreateElement("useremail");
			XmlNode userPassword = xmlDocument.CreateElement("userpassword");
			XmlAttribute number = xmlDocument.CreateAttribute("number");
			XmlAttribute numberPassword = xmlDocument.CreateAttribute("numberpass");
					   
			userID.InnerText = _newUserID;
			userEmail.InnerText = _userMail;
			userPassword.InnerText = _newUserPassword;

			int num = System.Convert.ToInt32(xmlDocument.SelectSingleNode("//totalaccountsnumber").InnerText);
			num++;
			number.InnerText = num.ToString();
			numberPassword.InnerText = num.ToString();

			account.AppendChild(userID);
			account.AppendChild(userEmail);
			account.AppendChild(userPassword);

			userID.Attributes.Append(number);
			userPassword.Attributes.Append(numberPassword);

			xmlDocument.SelectSingleNode("//totalaccountsnumber").InnerText = num.ToString();

			XmlNode accounts = xmlDocument.SelectSingleNode("//accounts");
			accounts.AppendChild(account);

			xmlDocument.Save("C:\\Users\\Adriana\\Documents\\artinfo-quizz\\ArtInfoQuizz\\ArtInfoQuizz\\bin\\Debug\\UserAccounts.xml");


		}

	}
}
